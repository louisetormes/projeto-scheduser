package br.com.scheduser.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.scheduser.entity.Agendamento;

public class AgendamentoDAO {
	
	public void criaAgendamento( List<Agendamento> registroAgendamento) {
		for( Agendamento agendamento:registroAgendamento ) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("dbscheduser");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			entityManager.getTransaction().begin();
			entityManager.persist( agendamento );
			entityManager.getTransaction().commit();
			
			entityManager.close();
			entityManagerFactory.close();
		}
	}
	
	public Agendamento consultaCodAgendamento( Integer codAgendamento ) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("dbscheduser");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		Agendamento agendamento = entityManager.find(Agendamento.class, codAgendamento);
		
		entityManager.close();
		entityManagerFactory.close();
		
		return agendamento;
	}

}
