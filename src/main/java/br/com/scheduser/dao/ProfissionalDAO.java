package br.com.scheduser.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.entity.Profissional;

public class ProfissionalDAO {

	public void criaProfissional( List<Profissional> registroProfissional) {
		for( Profissional profissional:registroProfissional ) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("dbscheduser");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			entityManager.getTransaction().begin();
			entityManager.persist( profissional );
			entityManager.getTransaction().commit();
			
			entityManager.close();
			entityManagerFactory.close();
		}
	}
	
	public Profissional consultaCpfProfissional( String cpfProfissional ) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("dbscheduser");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		Profissional profissional = entityManager.find(Profissional.class, cpfProfissional);
		
		entityManager.close();
		entityManagerFactory.close();
		
		return profissional;
	}

}
