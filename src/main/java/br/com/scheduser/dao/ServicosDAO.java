package br.com.scheduser.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.entity.Servicos;

public class ServicosDAO {
	
	public void criaServicos( List<Servicos> registroServicos) {
		for( Servicos servicos:registroServicos ) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("dbscheduser");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			entityManager.getTransaction().begin();
			entityManager.persist( servicos );
			entityManager.getTransaction().commit();
			
			entityManager.close();
			entityManagerFactory.close();
		}
	}
	
	public Servicos consultaCodServico( Integer codServico ) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("dbscheduser");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		Servicos servicos = entityManager.find(Servicos.class, codServico);
		
		entityManager.close();
		entityManagerFactory.close();
		
		return servicos;
	}


}
