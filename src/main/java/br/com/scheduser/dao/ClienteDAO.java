package br.com.scheduser.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.entity.Cliente;

public class ClienteDAO {

	public void criaCliente( List<Cliente> registroCliente) {
		for( Cliente cliente:registroCliente ) {
			EntityManagerFactory entityManagerFactory = Persistence
					.createEntityManagerFactory("dbscheduser");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			
			entityManager.getTransaction().begin();
			entityManager.persist( cliente );
			entityManager.getTransaction().commit();
			
			entityManager.close();
			entityManagerFactory.close();
		}
	}
	
	public Cliente consultaCpfCliente( String cpf ) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("dbscheduser");
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		Cliente cliente = entityManager.find(Cliente.class, cpf);
		
		entityManager.close();
		entityManagerFactory.close();
		
		return cliente;
	}

}
