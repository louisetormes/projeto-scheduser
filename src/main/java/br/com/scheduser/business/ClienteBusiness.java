package br.com.scheduser.business;

import java.util.List;
import java.util.Objects;

import br.com.scheduser.dao.ClienteDAO;
import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.entity.Cliente;
import br.com.scheduser.exceptions.IdNotExistException;
import br.com.scheduser.exceptions.RecordNullException;

public class ClienteBusiness {

private ClienteDAO daoCliente = new ClienteDAO();
	
	public void criaCliente( List<Cliente> registroCliente ) throws RecordNullException {
		if( Objects.isNull( registroCliente )) {
			throw new RecordNullException( "O registro de Cliente est� nulo!" );
		}
		this.daoCliente.criaCliente(registroCliente);
	}
	
	public Cliente consultaCpfCliente( String cpf ) throws IdNotExistException {
		if( cpf == null ) {
			throw new IdNotExistException( "CPF n�o encontrado!" );
		}
		return this.consultaCpfCliente(cpf);
}
}
