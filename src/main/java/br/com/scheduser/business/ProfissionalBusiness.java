package br.com.scheduser.business;

import java.util.List;
import java.util.Objects;

import br.com.scheduser.dao.ProfissionalDAO;
import br.com.scheduser.entity.Profissional;
import br.com.scheduser.exceptions.IdNotExistException;
import br.com.scheduser.exceptions.RecordNullException;

public class ProfissionalBusiness {

private ProfissionalDAO daoProfissional = new ProfissionalDAO();
	
	public void criaProfissional( List<Profissional> registroProfissional ) throws RecordNullException {
		if( Objects.isNull( registroProfissional )) {
			throw new RecordNullException( "O registro de Profissional est� nulo!" );
		}
		this.daoProfissional.criaProfissional(registroProfissional);
	}
	
	public Profissional consultaCpfProfissional( String cpfProfissional ) throws IdNotExistException {
		if( cpfProfissional == null ) {
			throw new IdNotExistException( "CPF n�o encontrado!" );
		}
		return this.consultaCpfProfissional(cpfProfissional);
}
}
