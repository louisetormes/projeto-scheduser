package br.com.scheduser.business;

import java.util.List;
import java.util.Objects;

import br.com.scheduser.dao.AgendamentoDAO;
import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.exceptions.IdNotExistException;
import br.com.scheduser.exceptions.RecordNullException;

public class AgendamentoBusiness {

	private AgendamentoDAO daoAgendamento = new AgendamentoDAO();
	
	public void criaAgendamento( List<Agendamento> registroAgendamento ) throws RecordNullException {
		if( Objects.isNull( registroAgendamento )) {
			throw new RecordNullException( "O registro de Agendamento est� nulo!" );
		}
		this.daoAgendamento.criaAgendamento(registroAgendamento);
	}
	
	public Agendamento consultaCodAgendamento( Integer codAgendamento ) throws IdNotExistException {
		if( codAgendamento == null ) {
			throw new IdNotExistException( "Id n�o encontrado!" );
		}
		return this.consultaCodAgendamento(codAgendamento);
}
}
