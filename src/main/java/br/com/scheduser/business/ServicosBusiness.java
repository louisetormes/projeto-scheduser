package br.com.scheduser.business;

import java.util.List;
import java.util.Objects;

import br.com.scheduser.dao.ServicosDAO;
import br.com.scheduser.entity.Servicos;
import br.com.scheduser.exceptions.IdNotExistException;
import br.com.scheduser.exceptions.RecordNullException;

public class ServicosBusiness {

private ServicosDAO daoServicos = new ServicosDAO();
	
	public void criaServicos( List<Servicos> registroServicos ) throws RecordNullException {
		if( Objects.isNull( registroServicos )) {
			throw new RecordNullException( "O registro de Servi�o est� nulo!" );
		}
		this.daoServicos.criaServicos(registroServicos);
	}
	
	public Servicos consultaCodServico( Integer codServico ) throws IdNotExistException {
		if( codServico == null ) {
			throw new IdNotExistException( "C�digo de Servi�o n�o encontrado!" );
		}
		return this.consultaCodServico(codServico);
}
}
