package br.com.scheduser.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "sch_servicos")
public class Servicos {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer codServico;
	
	@Column( nullable = false )
	private String nomeServico;
	
	public Servicos() {
		super();
	}
	
	public Servicos(Integer codServico, String nomeServico) {
		super();
		this.codServico = codServico;
		this.nomeServico = nomeServico;
	}

	public Integer getCodServico() {
		return codServico;
	}

	public void setCodServico(Integer codServico) {
		this.codServico = codServico;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}
}
