package br.com.scheduser.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "sch_agendamento")
public class Agendamento {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer codAgendamento;
	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn( name = "cliente_id" )
	private Cliente cliente;
	
	@Column
	private String dataAgendamento;
	
	@Column
	private String horaAgendamento;
	
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable( name = "sch_agendamento_servico", joinColumns = @JoinColumn(name = "agendamento_id"),
			inverseJoinColumns = @JoinColumn(name="servico_id"))
	private List<Servicos> servicoAgendamento;
	
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn( name = "profissional_id" )
	private Profissional profissionalAgendamento;

	public Agendamento() {
		super();
	}
	
	public Agendamento(Integer codAgendamento, Cliente cliente, String dataAgendamento, String horaAgendamento,
			List<Servicos> servicoAgendamento, Profissional profissionalAgendamento) {
		super();
		this.codAgendamento = codAgendamento;
		this.cliente = cliente;
		this.dataAgendamento = dataAgendamento;
		this.horaAgendamento = horaAgendamento;
		this.servicoAgendamento = servicoAgendamento;
		this.profissionalAgendamento = profissionalAgendamento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getDataAgendamento() {
		return dataAgendamento;
	}

	public void setDataAgendamento(String dataAgendamento) {
		this.dataAgendamento = dataAgendamento;
	}

	public String getHoraAgendamento() {
		return horaAgendamento;
	}

	public void setHoraAgendamento(String horaAgendamento) {
		this.horaAgendamento = horaAgendamento;
	}

	public List<Servicos> getServicoAgendamento() {
		return servicoAgendamento;
	}

	public void setServicoAgendamento(List<Servicos> servicoAgendamento) {
		this.servicoAgendamento = servicoAgendamento;
	}

	public Profissional getProfissionalAgendamento() {
		return profissionalAgendamento;
	}

	public void setProfissionalAgendamento(Profissional profissionalAgendamento) {
		this.profissionalAgendamento = profissionalAgendamento;
	}
	
}
