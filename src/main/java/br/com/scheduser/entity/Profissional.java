package br.com.scheduser.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table( name = "sch_profissional")
public class Profissional {

	@Id
	private String cpfProfissional;
	
	@Column
	private String nomeProfissional;
	
	@Column
	private String emailProfissional;
	
	@Column
	private String telefoneProfissional;
	
	@OneToMany( fetch = FetchType.LAZY )
	@JoinColumn( name = "profissional_id" )
	private List< Agendamento > agendamento;
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	@JoinTable( name = "sch_profissional_servicos", joinColumns = @JoinColumn(name = "profissional_id"),
	inverseJoinColumns = @JoinColumn(name="servico_id"))
	private List<Servicos> servicosProfissional;

	public Profissional() {
		super();
	}

	public Profissional(String cpfProfissional, String nomeProfissional, String emailProfissional,
			String telefoneProfissional, List<Agendamento> agendamento, List<Servicos> servicosProfissional) {
		super();
		this.cpfProfissional = cpfProfissional;
		this.nomeProfissional = nomeProfissional;
		this.emailProfissional = emailProfissional;
		this.telefoneProfissional = telefoneProfissional;
		this.agendamento = agendamento;
		this.servicosProfissional = servicosProfissional;
	}

	public String getCpfProfissional() {
		return cpfProfissional;
	}

	public void setCpfProfissional(String cpfProfissional) {
		this.cpfProfissional = cpfProfissional;
	}

	public String getNomeProfissional() {
		return nomeProfissional;
	}

	public void setNomeProfissional(String nomeProfissional) {
		this.nomeProfissional = nomeProfissional;
	}

	public String getEmailProfissional() {
		return emailProfissional;
	}

	public void setEmailProfissional(String emailProfissional) {
		this.emailProfissional = emailProfissional;
	}

	public String getTelefoneProfissional() {
		return telefoneProfissional;
	}

	public void setTelefoneProfissional(String telefoneProfissional) {
		this.telefoneProfissional = telefoneProfissional;
	}

	public List<Servicos> getServicosProfissional() {
		return servicosProfissional;
	}

	public void setServicosProfissional(List<Servicos> servicosProfissional) {
		this.servicosProfissional = servicosProfissional;
	}

	public List<Agendamento> getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(List<Agendamento> agendamento) {
		this.agendamento = agendamento;
	}
	
	
}
