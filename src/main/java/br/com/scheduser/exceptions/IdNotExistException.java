package br.com.scheduser.exceptions;

public class IdNotExistException extends Exception{

	public IdNotExistException( String mensagem ) {
		super(mensagem);
	}
}
