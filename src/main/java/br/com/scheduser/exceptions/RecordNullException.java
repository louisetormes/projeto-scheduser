package br.com.scheduser.exceptions;

public class RecordNullException extends Exception {
	
	public RecordNullException(String mensagem) {
		super(mensagem);
	}

}
